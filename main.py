#!/usr/bin/python
# -*- coding: utf-8 -*- 
# Импортируем необходимые моудли
import sys, os
import subprocess
import time
from PySide.QtGui import QApplication, QWidget, QMainWindow, QIcon, QPixmap, QFileDialog, QTableWidgetItem 
from PySide.QtCore import QTimer, SIGNAL
from db import Db 
from mainWindow import Ui_MainWindow
import psutil
import time

class MainWindow():
 	
	# Функция конструктора
	def __init__(self, myApp):
		self.window = QMainWindow()
		self.ui = Ui_MainWindow()
		self.window.setWindowIcon(QIcon(":/icon.png"))
		self.ui.setupUi(self.window)
		self.updatelist()
		self.setting = Db()
		timer = QTimer(myApp)
		timer.setInterval(10000) # interval in ms
		myApp.connect(timer, SIGNAL("timeout()"), self.updatelist)
		timer.start()

		self.ui.refresh.clicked.connect(self.updatelist)
		self.ui.kill.clicked.connect(self.killAll)
		self.ui.runStart.stateChanged.connect(self.save_runStart)
		self.ui.killClose.stateChanged.connect(self.save_killClose)
		self.ui.skype.editingFinished.connect(self.save_skype)
		self.ui.proceses.itemDoubleClicked.connect(self.killone)
		self.ui.skypeOpen.clicked.connect(self.setOpenFileName)
		self.ui.addRow.clicked.connect(self.addSkype)
		self.ui.delRow.clicked.connect(self.delSkype)
		self.ui.run.clicked.connect(self.run)
		
		self.loadSkypes()
		setting = self.setting.get_setting()

		for el in setting:
			if(el[0] == 'runOnStart'):
				self.ui.runStart.setChecked(el[1] == 'True')
			if(el[0] == 'killOnClose'):
				self.ui.killClose.setChecked(el[1] == 'True')
			if(el[0] == 'skype'):
				self.ui.skype.setText(el[1])

		if(self.ui.runStart.isChecked()):
			self.run()
			self.window.showMinimized()

	def setOpenFileName(self):
		fileName, _ = QFileDialog.getOpenFileName(self.window,
				"Место расположения Skype",
				"")
		if fileName:
			self.ui.skype.setText(fileName)
			self.save_skype()
	def delSkype(self):
		ids = set()
		rows = set()
		for item in self.ui.runlist.selectedItems():            
			ids.add(self.ui.runlist.item(item.row(), 0).text())
			rows.add(item.row())
		offset = 0
		for item in rows:
			self.ui.runlist.removeRow(item-offset)
			offset += 1
		
		for item in ids:
			self.setting.delete(item)            
			
	def run(self):
		skypes = self.setting.get_list()
		for el in skypes:
			cmd = ['/secondary', '/datapath:'+ el[1]]
			if (os.name == "posix"):
				cmd = ['--secondary', '--dbpath='+ el[1]]
			print(cmd)
			os.spawnvpe(os.P_NOWAIT, self.ui.skype.text(), cmd, os.environ);
			time.sleep(2)


	def loadSkypes(self):
		skypes = self.setting.get_list()
		for el in skypes:
			self.addTableRow(el[0],el[1])		
	def addSkype(self):
		dataDir = QFileDialog.getExistingDirectory(self.window,
				"Место расположения даных Skype",
				os.path.expanduser("~"))
		if dataDir:
			rid = self.setting.cteate(dataDir)
			self.addTableRow(rid,dataDir)

	def addTableRow(self, rid, dir):
		dir = QTableWidgetItem(dir)
		rid = QTableWidgetItem(str(rid))

		row = self.ui.runlist.rowCount()
		self.ui.runlist.setRowCount(row+1)
		self.ui.runlist.setItem(row,1,dir)
		self.ui.runlist.setItem(row,0,rid)

	def save_skype(self):
		self.setting.set_setting("skype", self.ui.skype.text())

	def save_runStart(self):
		self.setting.set_setting("runOnStart", str(self.ui.runStart.isChecked()))
		
	def save_killClose(self):
		self.setting.set_setting("killOnClose", str(self.ui.killClose.isChecked()))

	def updatelist(self):
		self.ui.proceses.clear()
		for proc in psutil.process_iter():
			try:
				pinfo = proc.as_dict(attrs=['pid', 'name'])
			except psutil.NoSuchProcess:
				pass
			else:
				if "skype" in pinfo['name'].lower():
					self.ui.proceses.addItem(str(pinfo['pid']) + ": " + pinfo['name'])


	def killAll(self):
		for proc in psutil.process_iter():
			try:
				pinfo = proc.as_dict(attrs=['pid', 'name'])
			except psutil.NoSuchProcess:
				pass
			else:
				if "skype" in pinfo['name'].lower():
					psutil.Process(pinfo['pid']).kill()
		self.updatelist()

	def killone(self):
		pid = int(self.ui.proceses.selectedItems()[0].text().split(":", 1)[0])
		print(pid)
		psutil.Process(pid).kill()
		self.updatelist()

if __name__ == '__main__':
	# Обработка исключений
	try:
		app = QApplication(sys.argv)
		
		mainWindow = MainWindow(app)
		mainWindow.window.show()
		
		icon = QIcon()
		icon.addPixmap(QPixmap("icon.png"), QIcon.Normal, QIcon.Off)
		app.setWindowIcon(icon)

		app.exec_()
		
		sys.exit(0)
	except NameError:
		print("Name Error:", sys.exc_info()[1])
	except SystemExit:
		if(mainWindow.ui.killClose.isChecked()):
			mainWindow.killAll()
		print("Closing Window...")
	except Exception:
		print (sys.exc_info()[1])