import sqlite3

class Db():
	def __init__(self):
		self.conn = sqlite3.connect('setting.db')
		self.create_base()

	def create_base(self):
		c = self.conn.cursor()
		c.execute('''CREATE TABLE IF NOT EXISTS skypes
             (id INTEGER PRIMARY KEY, dir text)''')
		self.conn.commit()
		c.execute('''CREATE TABLE IF NOT EXISTS setting
             (name text, value text)''')
		self.conn.commit()
		c.execute('''CREATE UNIQUE INDEX IF NOT EXISTS name_index on setting (name)''');
		self.conn.commit()
		c.execute('''INSERT INTO setting(name, value) SELECT "runOnStart", "false" 
				WHERE NOT EXISTS(SELECT 1 FROM setting WHERE name = "runOnStart");''')
		self.conn.commit()
		c.execute('''INSERT INTO setting(name, value) SELECT "killOnClose", "false" 
				WHERE NOT EXISTS(SELECT 1 FROM setting WHERE name = "killOnClose");''')
		self.conn.commit()
		c.execute('''INSERT INTO setting(name, value) SELECT "skype", "skype" 
				WHERE NOT EXISTS(SELECT 1 FROM setting WHERE name = "skype");''')
		self.conn.commit()


	def get_list(self):
		c = self.conn.cursor()
		c.execute('''SELECT * FROM skypes''')
		return c.fetchall()

	def cteate(self, dir):
		c = self.conn.cursor()
		c.execute('''INSERT INTO skypes(dir) VALUES (?)''', (dir,))
		id = c.lastrowid
		self.conn.commit()
		return id
	def delete(self, id):
		c = self.conn.cursor()
		c.execute('''DELETE FROM skypes WHERE (id=?)''', (id,))
		self.conn.commit()

	def get_setting(self):
		c = self.conn.cursor()
		c.execute('''SELECT * FROM setting''')
		return c.fetchall()

	def set_setting(self, name, value):
		c = self.conn.cursor()
		c.execute('''UPDATE setting
			SET value = "'''+ value +'''" WHERE name = "''' + name + '''"''')
		self.conn.commit()

	def update_skype(self, id, dir):
		c = self.conn.cursor()
		c.execute('''UPDATE skype
			SET dir = ? WHERE id = ?''', dir, id)
		self.conn.commit()
	